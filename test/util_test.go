package test

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"os"
	"standup-meeting/lib"
	"testing"
	"time"
)

// var client *model.Client4
var baseUrl, accessToken, teamName string

const apiRoot = "/api/v4/"

func Init() {
	_ = godotenv.Load()
	//client = http.Client{}
	teamName = os.Getenv("TEAM_NAME")
	//serverUrl := os.Getenv("SERVER_URL")
	//accessToken = os.Getenv("ACCESS_TOKEN")
	//baseUrl = fmt.Sprintf("%s%s", serverUrl, apiRoot)
	//client = model.NewAPIv4Client(serverUrl)
	//client.SetToken(accessToken)
}

func TestGetTeamUserList(t *testing.T) {
	Init()
	resp := lib.GetTeamUserList(teamName)
	//fmt.Println(resp)
	t.Log("TestGetTeamUserList ", prettyPrint(resp))
}

func TestCreateDirectChannel(t *testing.T) {
	channel := lib.CreateDirectChannel("tk4p46z86bbs5qeod6k77qix4e")

	post := lib.SendDirectMsg(channel.Id, lib.FirstQuestion)
	t.Log("TestCreateDirectChannel ", prettyPrint(post))

}

func TestTime(t *testing.T) {
	loc, _ := time.LoadLocation("Local")
	t1 := time.Now().In(loc)
	s1 := t1.Hour()

	fmt.Printf("s1: %d\n", s1)
}

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}
