package lib

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/mattermost/mattermost-server/v5/model"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

// 1. Get team id from team name 				/api/v4/teams/name/{name}
// 2. Get list of users id from team id 		/api/v4/teams/{team_id}/members
// 3. Get user info from user id				/api/v4/users/  body [ids]

// var client http.Client
var client *model.Client4
var (
	AccessToken string
	TeamName    string
	BaseUrl     string
	ServerUrl   string
	ServerWs    string
	TriggerAt   int
	ReportAt    int
)

const apiRoot = "/api/v4/"

func init() {
	_ = godotenv.Load()
	//client = http.Client{}
	TeamName = os.Getenv("TEAM_NAME")
	ServerUrl = os.Getenv("SERVER_URL")
	ServerWs = getWsUrl(ServerUrl)
	AccessToken = os.Getenv("ACCESS_TOKEN")
	TriggerAt, _ = strconv.Atoi(os.Getenv("DAILY_TRIGGER_AT"))
	ReportAt, _ = strconv.Atoi(os.Getenv("DAILY_REPORT_AT"))
	BaseUrl = fmt.Sprintf("%s%s", ServerUrl, apiRoot)
	client = model.NewAPIv4Client(ServerUrl)
	client.SetToken(AccessToken)
	fmt.Println(TeamName)
	fmt.Println(ServerUrl)
	fmt.Println(AccessToken)
}

func GetTeamUserList(teamName string) []*model.User {
	team, response := client.GetTeamByName(teamName, "")
	if response.Error != nil {
		log.Fatalf("Error getting team: %v", response.Error)
	}
	log.Println("Get Team: ", prettyPrint(team))
	members, response := client.GetTeamMembers(team.Id, 0, 100, "")
	var ids []string
	for _, member := range members {
		ids = append(ids, member.UserId)
	}
	users, _ := client.GetUsersByIds(ids)
	if response.Error != nil {
		log.Fatalf("Error getting team members: %v", response.Error)
	}
	return users
}

func CreateDirectChannel(userId string) *model.Channel {
	botUser, _ := client.GetMe("")
	channel, _ := client.CreateDirectChannel(botUser.Id, userId)
	return channel
}

func SendDirectMsg(chanId, msg string) *model.Post {
	//botUser, _ := client.GetMe("")
	post := createQuestionPost(msg, chanId)
	p, _ := client.CreatePost(post)
	return p
}

func GetTime() (int, int) {
	loc, _ := time.LoadLocation("Local")
	t1 := time.Now().In(loc)
	return t1.Hour(), t1.Day()
}

func createQuestionPost(msg, channelId string) *model.Post {
	props := map[string]interface{}{
		"attachments": []model.SlackAttachment{
			{
				Fallback:   "",
				Color:      "#107baf",
				AuthorName: "DailyBot",
				AuthorLink: "",
				AuthorIcon: "",
				Title:      "Daily Meeting",
				Text:       msg,
			},
		},
	}
	post := model.Post{
		ChannelId: channelId,
		Message:   "",
	}
	post.SetProps(props)
	return &post
}

func createReport() {

}

func getWsUrl(url string) string {
	s := strings.Split(url, ":")
	return "ws:" + s[1] + ":" + s[2]
}

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}
