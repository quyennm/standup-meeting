package lib

const (
	MatterMostEvent = 1
	LocalEvent      = 2
)

const (
	FirstQuestion  = "What did you accomplish yesterday?"
	SecondQuestion = "What will you do today?"
	ThirdQuestion  = "What (if anything) is blocking your progress?"
)

type BotEvent struct {
	Data interface{}
	Type int
}
