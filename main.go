package main

import (
	"encoding/json"
	"fmt"
	"github.com/mattermost/mattermost-server/v5/model"
	"log"
	"standup-meeting/lib"
	"time"
)

var isRunning bool
var running map[string]bool
var quit = make(chan struct{})
var report = make(map[string]model.SlackAttachmentField)

func main() {
	var lastDay int
	fmt.Println("Start Daily Bot")
	go startWS()
	ticker := time.NewTicker(5 * time.Second)
	//quit = make(chan struct{})
	time.Now().UTC()
	for {
		select {
		case <-ticker.C:
			hour, day := lib.GetTime()
			if !isRunning || (isRunning && hour == lib.TriggerAt && day != lastDay) {
				lastDay = startDaily()
			}
		case <-quit:
			ticker.Stop()
			return
		}
	}
}

func startWS() {
	wsClient, err := model.NewWebSocketClient4(lib.ServerWs, lib.AccessToken)
	if err != nil {
		fmt.Println("New WSClient Failed ", err)
		quit <- struct{}{}
	}
	err = wsClient.Connect()
	if err != nil {
		fmt.Println("Connect to ws Failed ", wsClient.ConnectUrl)
		quit <- struct{}{}
	}
	wsClient.Listen()
	for {
		select {
		case event := <-wsClient.EventChannel:
			handleEventChannel(event)
		}
	}
}

func startDaily() int {
	log.Println("Start Daily")
	isRunning = true
	_, day := lib.GetTime()
	users := lib.GetTeamUserList(lib.TeamName)
	for _, user := range users {
		if !running[user.Id] {
			go startUserDaily(user)
		}
	}
	return day
}

func startUserDaily(user *model.User) {
	log.Println("Start Daily for User ", user.Username)
	//client := model.NewAPIv4Client(lib.ServerUrl)
	channel := lib.CreateDirectChannel(user.Id)

	botChan := make(chan lib.BotEvent)
	startEvent := lib.BotEvent{
		Data: lib.FirstQuestion,
		Type: lib.LocalEvent,
	}
	botChan <- startEvent
	for {
		select {
		case event := <-botChan:
			fmt.Println("Receive Event ", event.Type, event.Data)
			switch event.Type {
			case lib.LocalEvent:
				lib.SendDirectMsg(channel.Id, event.Data.(string))
			case lib.MatterMostEvent:
				fmt.Println()
			}
		}
	}
}

func handleEventChannel(ev *model.WebSocketEvent) {
	log.Println("Handle event ", ev.EventType(), prettyPrint(ev.GetData()))
}

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}
